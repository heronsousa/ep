Rails.application.routes.draw do
  resources :categories
  resources :authors
  devise_for :users
  get 'inicial/index'
  root 'inicial#index'
  resources :books
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

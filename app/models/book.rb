class Book < ApplicationRecord
	belongs_to :user
	belongs_to :category, :optional => true
	belongs_to :author, :optional => true

	def self.search(search)
  		where("titulo LIKE ?", "%#{search}%") 
	end

	has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end

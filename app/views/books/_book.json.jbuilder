json.extract! book, :id, :titulo, :descricao, :opcao, :categoria_id, :autor_id, :created_at, :updated_at
json.url book_url(book, format: :json)

class RemoveAutorIdFromBook < ActiveRecord::Migration[5.2]
  def change
    remove_column :books, :autor_id, :integer
  end
end

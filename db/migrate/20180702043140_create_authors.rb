class CreateAuthors < ActiveRecord::Migration[5.2]
  def change
  	drop_table :authors
    create_table :authors do |t|
      t.string :name

      t.timestamps
    end
  end
end

class RemoveCategoriaIdFromBook < ActiveRecord::Migration[5.2]
  def change
    remove_column :books, :categoria_id, :integer
  end
end

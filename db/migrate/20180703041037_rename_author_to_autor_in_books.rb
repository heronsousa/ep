class RenameAuthorToAutorInBooks < ActiveRecord::Migration[5.2]
  def change
  	rename_column :books, :author, :autor
  end
end
